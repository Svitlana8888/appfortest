//
//  ViewController.swift
//  CORE_DATA_APP
//
//  Created by Alok Deepti on 17/07/19.
//  Copyright © 2019 Alok. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tblview: UITableView!
    var datasourceArray = [Person]()
    let appDelegete = UIApplication.shared.delegate as! AppDelegate
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchAndUpdateTable()
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            let person = datasourceArray[indexPath.row]
            appDelegete.deleteRecord(person: person)
            fetchAndUpdateTable()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let person = datasourceArray[indexPath.row]

        
        var nameTextField: UITextField?
        var addressTextField: UITextField?
        
        
        // Declare Alert message
        let dialogMessage = UIAlertController(title: "Alert Title", message: "Please update name and address", preferredStyle: .alert)
        
        // Create OK button with action handler
        let ok = UIAlertAction(title: "UPDATE", style: .default, handler: { (action) -> Void in
            
            let name = nameTextField?.text
            let address = addressTextField?.text
            
            if name != nil && address != nil{
                
                self.appDelegete.updateRecord(person: person, name: name!, address: address!)
                self.fetchAndUpdateTable()
            }
            
        })
        
        // Create Cancel button with action handlder
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
            print("Cancel button tapped")
        }
        
        //Add OK and Cancel button to dialog message
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)
        
        // Add Input TextField to dialog message
        dialogMessage.addTextField { (textField) -> Void in
            
            nameTextField = textField
            nameTextField?.placeholder = "Type in your name"
            nameTextField?.text = person.name
        }
        
        dialogMessage.addTextField { (textField) -> Void in
            
            addressTextField = textField
            addressTextField?.placeholder = "Type in your address"
            addressTextField?.text = person.address
        }
        
        // Present dialog message to user
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasourceArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        let person = datasourceArray[indexPath.row]
        cell?.textLabel?.text = person.name! + " " + person.address!
        return cell!
    }
    
    func fetchAndUpdateTable(){
        datasourceArray = appDelegete.fetchRecords()
        tblview.reloadData()
    }
    
    @IBAction func addRecord(_ sender: UIButton) {
        
        var nameTextField: UITextField?
        var addressTextField: UITextField?
        
        
        // Declare Alert message
        let dialogMessage = UIAlertController(title: "Alert Title", message: "Please provide name and address", preferredStyle: .alert)
        
        // Create OK button with action handler
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            
            let name = nameTextField?.text
            let address = addressTextField?.text
            
            if name != nil && address != nil{
                
                self.appDelegete.insertRecord(name: name!, address: address!)
                self.fetchAndUpdateTable()
            }
            
        })
        
        // Create Cancel button with action handlder
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
            print("Cancel button tapped")
        }
        
        //Add OK and Cancel button to dialog message
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)
        
        // Add Input TextField to dialog message
        dialogMessage.addTextField { (textField) -> Void in
            
            nameTextField = textField
            nameTextField?.placeholder = "Type in your name"
        }
        
        dialogMessage.addTextField { (textField) -> Void in
            
            addressTextField = textField
            addressTextField?.placeholder = "Type in your address"
        }
        
        // Present dialog message to user
        self.present(dialogMessage, animated: true, completion: nil)
        
    }
    

}

